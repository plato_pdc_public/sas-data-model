# SAS Data model

Data model for SAS :

- pipeline
- HDF5
- ddd

## Generate both the SAS data products and the PLATO ddd from the YAML file

```bash
pip install -r requirements.txt
sh scripts/generate_schema.sh
```

## Generate the documentation

```bash
pip install -r requirements_doc.txt
sh scripts/generate_doc.sh
```

## Generate the API for py_sas_data_modeler

```bash
pip install -r requirements_dm.txt
sh scripts/generate_sas_py.sh
```

## GitLab CI Pipeline Diagram

```mermaid
graph TD
    %% Triggers
    A([run_script_on_yaml_change]) -->|Trigger: Changes in DPdoc_with_dm.yaml| B([generate_sas_file])
    B -->|Trigger: Changes in json-schema/*| C([create_mr_in_py_sas_data_modeler])
    
    %% Parallel trigger for pages
    B -->|Trigger: Tags| D([pages])

    %% Actions for each stage
    A -->|Action: Run generate_schema.sh| B
    B -->|Action: Create sas.py artifact| C
    C -->|Action: Clone, commit, push changes, create MR|pySasDataModeler
    D -->|Action: Deploy docs to GitLab Pages|Website

    %% Styling
    style A fill:#a0c4ff,stroke:#333,stroke-width:2px;
    style B fill:#ff9b54,stroke:#333,stroke-width:2px;
    style C fill:#ffd166,stroke:#333,stroke-width:2px;
    style D fill:#06d6a0,stroke:#333,stroke-width:2px;
```