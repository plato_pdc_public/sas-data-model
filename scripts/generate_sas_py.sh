#!/bin/sh

# Display help
show_help() {
  echo "Usage: $0 [OPTIONS]"
  echo ""
  echo "Options:"
  echo "  --debug    Add the optionsv--debug to the datamodel-codegen command."
  echo "  --help     Display this help message."
}

# Init the options
DEBUG_MODE=false

# Process arguments
while [ $# -gt 0 ]; do
  case "$1" in
    --debug)
      DEBUG_MODE=true
      shift
      ;;
    --help)
      show_help
      exit 0
      ;;
    *)
      echo "Unknown option: $1"
      show_help
      exit 1
      ;;
  esac
done

# Build the commande line
mkdir -p dm
CMD="datamodel-codegen --http-ignore-tls --input json-schema/sas_schema.json --input-file-type jsonschema --output dm/sas.py --output-model-type pydantic_v2.BaseModel --enable-version-header --custom-template-dir template/ --allow-population-by-field-name --use-field-description --custom-file-header-path ./extras/header.txt --additional-imports \"typing_extensions.Annotated\" --base-class py_sas_data_modeler.dm.dataclass_management.BaseDataClass --additional-imports py_sas_data_modeler.dm.dataclass_management.BaseRootModel --use-annotated"

# Display --debug options if set
if [ "$DEBUG_MODE" = true ]; then
  CMD="$CMD --debug"
fi

# Execute the command
eval $CMD
