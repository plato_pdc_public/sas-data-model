from typing import List, Optional, Dict, Any, Tuple, Union
import logging
import json
import os

import yaml
from pydantic import BaseModel, Field, field_validator
import click
import jsonschema

VERSION = "1.0.0"
SCHEMA_VERSION = "https://json-schema.org/draft/2019-09/schema"
MAPPING_UNICODE = {
    '\\u2299':"⊙",
    '\xB5':"µ",
    '\xB2':"²",
    '\\u2212':"-",
    '\\u03BE':"ϰ",
    '\\u03B6':"ζ",
    '\\u2019':"’",
    '\\u00b5':"µ",
    '\\u00b2':"²",
    '\\u2013':"-",
    '\\u03BD':"ν",
    '\\u03bd':'ν',
    '\\u00a0':' ',
    '\\u03bd':'ν',
    '\\u03c4':'τ',
    '\\u2206':'∆',
    '\\u03bd':'ν',
    '\\u03b1':'α' ,
    '\\u03b6':'ζ',
    '\\ud835\\udf122':'𝜒2'     
}
YAML_TYPE_MAAPING = {
    "float64": "number",
    "int64": "integer",
    "string": "string",
    "bool": "boolean",
    "Boolean": "boolean",
    "boolean": "boolean",
}

class Utils:
    @staticmethod
    def replace_unicode(value: str):
        for key, replacement in MAPPING_UNICODE.items():
            value = value.replace(key, replacement)
        return value    
    
    @classmethod
    def fix_filename(cls, filename: str) -> str:
        """
        Fixes the filename by removing "[" and "]" characters.
        """
        filename = filename.replace("[", "")
        filename = filename.replace("]", "")
        return filename    

    @staticmethod
    def normalize_file_path(file: str) -> str:
        if os.path.isabs(file):
            return file
        else:
            user_path = os.path.abspath(os.path.curdir)
            return os.path.realpath(os.path.join(user_path, file)) 
        
class CustomFormatter(logging.Formatter):
    """Logging colored formatter, adapted from https://stackoverflow.com/a/56944256/3638629"""

    grey = "\x1b[38;21m"
    blue = "\x1b[38;5;39m"
    yellow = "\x1b[38;5;226m"
    red = "\x1b[38;5;196m"
    bold_red = "\x1b[31;1m"
    reset = "\x1b[0m"

    def __init__(self, fmt):
        super().__init__()
        self.fmt = fmt
        self.FORMATS = {
            logging.DEBUG: self.grey + self.fmt + self.reset,
            logging.INFO: self.blue + self.fmt + self.reset,
            logging.WARNING: self.yellow + self.fmt + self.reset,
            logging.ERROR: self.red + self.fmt + self.reset,
            logging.CRITICAL: self.bold_red + self.fmt + self.reset,
        }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)


# Create custom logger logging all five levels
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# Define format for logs
fmt = "%(message)s"

# Create stdout handler for logging to the console (logs all five levels)
stdout_handler = logging.StreamHandler()
stdout_handler.setLevel(logging.DEBUG)
stdout_handler.setFormatter(CustomFormatter(fmt))

# Add both handlers to the logger
logger.addHandler(stdout_handler)


class UnknownException(Exception):
    pass



class UnknownDimensionException(Exception):
    pass


class OutOfBaseLineException(Exception):
    pass


class Column(BaseModel):
    """
    Processing of YAML column

    Attributes:
        title (str): The column name of the YAML file.
        description (Optional[str]): Description from the YAML file.
        type (str): Conversion of the datatype from the YAML file.
        unit (Optional[str]): Unit from the YAML file converted in UTF-8.
        sub_column (Optional[Column]): Sub column when deep object.

    Methods:
        model_post_init(self, __context: Any) -> None:
            Handle deep object for column hierarchy.

        strip_whitespace(cls, value: Any) -> Any:
            Validates and strips whitespace from string values.

        name_must_not_be_empty(cls, value) -> Any:
            Validates that the name attribute is not empty.

        unicode_translation(cls, value) -> Any:
            Translates Unicode characters.

        _build_attributes_to_exclude(self) -> List[str]:
            Builds attributes to exclude from the representation.

        _convert_to_json_datatype(self, min: float = None, max: float = None) -> Dict:
            Converts YAML to JSON datatype.

        model_dump(self) -> Dict:
            Dumps the column model to a dictionary.

        _to_dict_datatype(self, yaml_type: str, dimension: int) -> Dict:
            Converts YAML type to JSON datatype.
    """

    title: str = Field(
        ..., alias="Name", description="Title is the column name of the YAML file"
    )
    description: Optional[str] = Field(
        None, alias="Description", description="Description from the YAML file"
    )
    type: str = Field(
        ..., alias="Type", description="Convertion of the datatype from the YAML file"
    )
    unit: Optional[str] = Field(
        None, alias="Unit", description="Unit from the YAML file converted in UTF-8"
    )
    minimum: Optional[Union[float, int]] = Field(
        None, alias="minimum", description="Minimum value"
    )
    maximum: Optional[Union[float, int]] = Field(
        None, alias="maximum", description="Maximum value"
    )    
    sub_column: Optional["Column"] = Field(
        None, description="Sub column when deep object"
    )
    is_optional: Optional[bool] = Field(default=False, exclude=True)
    
    no_data_value: Optional[Union[float, int, str]] = Field(
        default=None,
        description="Default value for a missing value", 
        exclude=True
    )

    def model_post_init(self, __context: Any) -> None:
        """
        Handle nested object for column hierarchy.
        """
        self._handle_sub_column()
        self._normalize_type()
        self._update_description_with_unit() 
            
    def _handle_sub_column(self) -> None:
        """Handles the sub_columns creation when a column title contains '/'. """
        parts = self.title.split("/")
        if len(parts) > 1:
            self.sub_column = Column(Name="/".join(parts[1:]), Type=self.type, minimum=self.minimum, maximum=self.maximum)
            self.type = "object"
            self.title = parts[0].strip()

    def _normalize_type(self) -> None:
        """Normalize the type by adding ',0' when no dimension."""
        if self.type == "":
            pass        
        elif "," not in self.type and self.type == "object":
            self.type = f"{self.type},0"
        elif "," not in self.type:
            logger.warning(f"Type syntax problem for {self.title} with {self.type}, assuming not an array")
            self.type = f"{self.type},0"

    def _update_description_with_unit(self) -> None:
        """Add a unit in the description when it exists."""
        if self.unit:
            self.description = f"{self.description} in {self.unit}" if self.description else self.unit
                             
    @field_validator("*", mode="before")
    def _strip_whitespace(cls, value: Any) -> Any:
        """
        Validates and strips whitespace from string values.
        """
        if isinstance(value, str):
            return value.strip()
        return value

    @field_validator("title")
    def _name_must_not_be_empty(cls, value) -> Any:
        """
        Validates that the name attribute is not empty.
        """
        if not value:  # check if the string is empty
            raise ValueError("Name cannot be an empty string")
        return value

    def _build_attributes_to_exclude(self) -> List[str]:
        """Builds attributes to exclude from the representation."""
        exclude = ["unit", "sub_column"]
        if not self.description:
            exclude.append("description")
        if self.minimum is None or ",0" not in self.type or self.sub_column: 
            exclude.append("minimum")
        if self.maximum is None or ",0" not in self.type or self.sub_column:
            exclude.append("maximum")    
            

        return exclude

    def _convert_to_json_datatype(self, min: float = None, max: float = None) -> Dict:
        """
        Converts YAML to JSON datatype.
        """
        yaml_type, dimension = self.type.split(",")
        yaml_type = YAML_TYPE_MAAPING.get(yaml_type, yaml_type)
        return self._to_dict_datatype(yaml_type, dimension)

    def model_dump(self) -> Dict:
        """
        Dumps the column model to a dictionary.
        """
        
        # Skip if no type found
        if not self.type:
            logger.warning(f"Type not found, skip the column for {self.title}")
            return {}

        exclude = self._build_attributes_to_exclude()
        dump = super().model_dump(exclude=exclude)

        if self.sub_column:
            model: Dict = self.sub_column.model_dump()
            dump["properties"] = {model["title"]: model}
            dump["required"] = [model["title"]]
            dump["additionalProperties"] = False

        datatype = self._convert_to_json_datatype()
        dump.update(datatype)
        return dump

    def _to_dict_datatype(self, yaml_type: str, dimension: int) -> Dict:
        """
        Converts YAML type to JSON datatype.
        """
        dimension = int(dimension) if dimension else 0

        if dimension not in {0, 1, 2, 3}:
            raise UnknownDimensionException(dimension)

        yaml_type = yaml_type if self.no_data_value is None else [yaml_type, "null"]
        json_data_type = {"type": yaml_type} if dimension == 0 else {
            "type": "array",
            "items": {"type": yaml_type, **({"minimum": self.minimum} if self.minimum is not None else {}),
                                          **({"maximum": self.maximum} if self.maximum is not None else {})}
        }

        for _ in range(dimension - 1):
            json_data_type = {"type": "array", "items": json_data_type}                  

        return json_data_type
    
    def model_dump_json(self, **args) -> str:
        """
        Dumps the data product model to a JSON string.
        
        Args:
            **args: Additional keyword arguments.
            
        Returns:
            str: A JSON string representing the dumped data product model.
        """
        return json.dumps(self.model_dump(), indent=2, sort_keys=True)       


class SAS_DP(BaseModel):
    """
    Represents a data product provided by SAS pipeline.
    
    Attributes:
        title (str): The title of the data product.
        description (str, optional): The description of the data product.
        reference (str, optional): The reference of the data product.
        related_to (str, optional): Related information to the data product.
        properties (List[Column]): List of columns representing properties.
        additionalProperties (bool): Additional properties flag.
        required (List[str]): List of required attributes.    
    """
    id: str = None
    schema: str = SCHEMA_VERSION
    version: str = VERSION
    type: str = "object"
    title: str = Field(..., alias="name")
    description: Optional[str] = Field(..., alias="Description")
    reference: Optional[str] = Field(..., alias="Reference")
    related_to: Optional[str] = Field(..., alias="Related to")
    columns: List[Column] = Field(..., alias="Columns")
    properties: Dict = None
    additionalProperties: bool = False
    required: List[str] = None

    def model_post_init(self, __context: Any) -> None:
        """
        Initializes the required properties based on the columns.
        All attributes are required.
        
        Args:
            __context (Any): Contextual information, not used.
            
        Returns:
            None
        """
        self.required = list(set([column.title for column in self.columns if column.is_optional is False]))
                
        filename: str = Utils.fix_filename(self.title.lower() + "_schema.json")
        self.id = f"https://plato_pdc_public.io.ias.u-psud.fr/sas-data-model/data_products/{filename}"
        
        self.properties = self._build_properties()


    @field_validator("*", mode="before")
    def _strip_whitespace(cls, value: Any) -> Any:
        """
        Validates and strips whitespace from string values.
        
        Args:
            value (Any): The value to be validated and processed.
            
        Returns:
            Any: The processed value.
        """
        if isinstance(value, str):
            return value.strip()
        return value

    @field_validator("reference")
    def _skip_out_of_baseline(cls, value) -> Any:
        """
        Validates and skips "out of baseline" references.
        
        Args:
            value (Any): The value to be validated.
            
        Returns:
            Any: The validated value.
            
        Raises:
            OutOfBaseLineException: If the value is "out of baseline".
        """
        if isinstance(value, str) and value.strip() == "out of baseline":
            raise OutOfBaseLineException()
        return value   

    def _build_attributes_to_exclude(self) -> List[str]:
        """
        Builds a list of attributes to exclude from the model dump.
        
        Returns:
            List[str]: A list of attribute names to be excluded.
        """
        exclude = ["columns"]
        if not self.description:
            exclude.append("description")
        if not self.reference:
            exclude.append("reference")
        if not self.related_to:
            exclude.append("related_to")
        return exclude

    def _update_nested_dict(self, source: Dict, dest: Dict):
        """
        Updates a nested dictionary with source values.
        
        Args:
            source (Dict): The source dictionary containing values to be updated.
            dest (Dict): The destination dictionary to be updated.
        """
        for key, value in source.items():
            if isinstance(value, dict) and key in dest and isinstance(dest[key], dict):
                self._update_nested_dict(value, dest[key])
            elif key in dest and key == "required":
                dest[key].extend(source[key])
                dest[key] = list(set(dest[key]))
            else:
                dest[key] = source[key]
                
    def _build_properties(self) -> Dict:
        """
        Builds dictionary of properties.
        
        Returns:
            Dict: A dictionary representing the built properties.
        """        
        properties_dict = dict()
        for column in self.columns:
            self._update_nested_dict(
                {column.title: column.model_dump()}, properties_dict
            )
        return properties_dict        

    def model_dump(self, **args) -> Dict:
        """
        Dumps the data product model to a dictionary.
        
        Args:
            **args: Additional keyword arguments.
            
        Returns:
            Dict: A dictionary containing the dumped data product model.
        """
        data_dict = super().model_dump(exclude=args["exclude"])
        data_dict["$schema"] = data_dict["schema"]
        data_dict["$id"] = data_dict["id"]
        del data_dict["schema"]
        del data_dict["id"]
        return data_dict

    def model_dump_json(self, **args) -> str:
        """
        Dumps the data product model to a JSON string.
        
        Args:
            **args: Additional keyword arguments.
            
        Returns:
            str: A JSON string representing the dumped data product model.
        """
        exclude = self._build_attributes_to_exclude()
        return json.dumps(self.model_dump(exclude=exclude), indent=2, sort_keys=True)
    
class Yaml:
    """
    Represents a YAML file containing SAS data product definitions.

    Attributes:
        __yamfile (str): Normalized path to the YAML file.
        __products (Dict[str, SAS_DP]): Dictionary containing SAS data products.
    """    
    def __init__(self, yamlfile: str) -> None:
        """
        Initializes the YAML object.

        Args:
            yamlfile (str): Path to the YAML file.

        Returns:
            None
        """        
        self.__yamfile: str = Utils.normalize_file_path(yamlfile)
        self.__products: Dict[str, SAS_DP] = self._load_dp()
     
    @property    
    def yamfile(self):
        """
        Returns the path to the YAML file.
        """        
        return self.__yamfile
    
    @property
    def products(self):
        """
        Returns the dictionary containing SAS data products.
        """        
        return self.__products
        
    def _load_content(self) -> Any:
        """
        Loads the content of the YAML file.

        Returns:
            Any: Content of the YAML file.
        """        
        yaml_content: Any
        with open(self.yamfile, "r", encoding="utf-8") as file:
            yaml_content = yaml.safe_load(file)  
        return yaml_content              
            
    def _load_dp(self) -> Dict[str, SAS_DP]:
        """
        Loads SAS data products from the YAML file.

        Returns:
            Dict[str, SAS_DP]: Dictionary containing SAS data products.
        """        
        yaml_content: Any = self._load_content()    
        
        def create_sas_dp(product_name, product_def):
            try:
                return SAS_DP(name=product_name, **product_def)
            except OutOfBaseLineException as err:
                logger.warning(f"{product_name} is out of base line, skip it")
                pass          

        # extract the list of DPs from the YAML
        data_structure = [
            create_sas_dp(product_name, product_def)
            for cat in yaml_content.values()
            if isinstance(cat, dict)
            for key, ds in cat.items()
            if key in ['Data-Structure(s)']
            for dp in ds
            for product_name, product_def in dp.items()
            if create_sas_dp(product_name, product_def) is not None
        ]  
        metadata_structure = [
            create_sas_dp(product_name, product_def)
            for cat in yaml_content.values()
            if isinstance(cat, dict)
            for key, ds in cat.items()
            if key in ['Meta-Data-Structure(s)', 'Metadata-Structure(s)']
            for dp in ds
            for product_name, product_def in dp.items()
            if create_sas_dp(product_name, product_def) is not None
        ]
        quality_structure =  [
            create_sas_dp(product_name, product_def)
            for cat in yaml_content.values()
            if isinstance(cat, dict)
            for key, ds in cat.items()
            if key in ['Quality-Control-Structure(s)']
            for dp in ds
            for product_name, product_def in dp.items()
            if create_sas_dp(product_name, product_def) is not None
        ]   
        return {
            'data':data_structure,
            'metadata': metadata_structure,
            'quality': quality_structure
        }   
             

    def write_dp_json_schema(self, output_dir: str):
        output_path: str = Utils.normalize_file_path(output_dir)
        os.makedirs(output_path, exist_ok=True)
        dps = [dp for _, dp_in_structure in self.products.items() for dp in dp_in_structure]
        for dp in dps:
            schema_name: str = f"{dp.title.lower()}_schema.json"
            logger.info(f"Writing {schema_name}")
            filename: str = Utils.fix_filename(schema_name)
            filepath: str = os.path.join(output_path, filename)
            with open(filepath, 'w') as file:
                json_schema = Utils.replace_unicode(dp.model_dump_json(indent=2))
                jsonschema.Draft202012Validator.check_schema(json.loads(json_schema)) 
                file.write(json_schema)   
                
        logger.info(f"{len(dps)} JSON schema have been created")
                         
    
    def display_dp_json_schema(self):
        dps = [dp for _, dp_in_structure in self.products.items() for dp in dp_in_structure]
        for dp in dps:
            filename = Utils.fix_filename(dp.title.lower()+"_schema.json")
            json_schema: str = Utils.replace_unicode(dp.model_dump_json(indent=2))
            jsonschema.Draft202012Validator.check_schema(json.loads(json_schema))            
            display = f"""
        filename: {filename}
        {'-'*(len(filename)+10)}
        {json_schema}
        """
            logger.info(display)   
        
        logger.info(f"{len(dps)} JSON schema have been created")
        

@click.command()
@click.option('--yaml-file', '-f', default='DPdoc_with_dm.yaml', help='Name of the YAML file.', show_default=True)
@click.option('--output-dir', '-o', default='./output', help='Output directory', show_default=True)
@click.option('--dry-run', is_flag=True, help='Simulation mode')
def sas(yaml_file, output_dir, dry_run):
    """Command to generate SAS data products"""
    yaml_data = Yaml(yaml_file)
    if dry_run:
        yaml_data.display_dp_json_schema()
    else:
        yaml_data.write_dp_json_schema(output_dir)  
    
if __name__ == '__main__':
    sas()

