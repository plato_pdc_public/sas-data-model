import subprocess
import pytest
from pathlib import Path

def test_generate_classes_sh():
    """Test for checking the shell script generate the classes correctly."""

    # Run the script shell
    result = subprocess.run(
        ["bash", str(Path("scripts/generate_sas_py.sh"))],
        capture_output=True,
        text=True
    )
    
    sas_path = Path("dm/sas.py")

    # Check the script is executed without an error
    assert result.returncode == 0, f"The script has failed : {result.stderr}"
    assert sas_path.exists()


