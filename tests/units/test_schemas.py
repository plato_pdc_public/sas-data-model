import jsonschema
import os
import json
import pytest
import logging
    
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


@pytest.fixture(scope="class")
def my_class_instance():
    logger.info("Setting up TestSchemas instance")
    my_instance = TestSchemas()
    yield my_instance
    logger.info("Tearing down TestSchemas instance")
    del my_instance 
    
class TestSchemas:

    def _validate_schema(self, file_path):
        is_valid = False
        with open(file_path, 'r') as file:
            schema_data = json.load(file)
        try:
            jsonschema.Draft7Validator.check_schema(schema_data)
            is_valid =  True
        except jsonschema.exceptions.SchemaError as e:
            print(f"Schema {file_path} is not valid: {e}")
            is_valid = False
        return is_valid
    
    def _validate_all_schemas_in(self, directory):
        for filename in os.listdir(directory):
            if filename.endswith('.json'):
                file_path = os.path.join(directory, filename)
                assert self._validate_schema(file_path), f"Schema '{file_path}' is not valid."        

    def test_validate_schemas_data_products(self, my_class_instance):
        directory_path = 'json-schema/data_products'
        logger.info(f"test validate_schemas_data_products on directory {directory_path}")  
        self._validate_all_schemas_in(directory_path)      
                
        
    def test_validate_schemas_external(self, my_class_instance):
        directory_path = 'json-schema/external'
        logger.info(f"test alidate_schemas_external on directory {directory_path}") 
        self._validate_all_schemas_in(directory_path)                

